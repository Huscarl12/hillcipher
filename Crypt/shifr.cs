﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Tracing;

namespace Crypt
{
    public class shifr
    {
        public static readonly List<char> alphabet = new List<char>()
        {
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Ч','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я','.',',',' ','?'
        };

        public string word;
        public string key;

        public shifr()
        {
            
        }
        
        static int[,] Multiplication(int[,] a, int[,] b)
        {
            if (a.GetLength(1) != b.GetLength(0)) throw new Exception("Матрицы нельзя перемножить");
            var r = new int[a.GetLength(0), b.GetLength(1)];
            for (var i = 0; i < a.GetLength(0); i++)
            {
                for (var j = 0; j < b.GetLength(1); j++)
                {
                    for (var k = 0; k < b.GetLength(0); k++) r[i, j] += a[i, k] * b[k, j];
                }
            }
            return r;
        }
    }
}